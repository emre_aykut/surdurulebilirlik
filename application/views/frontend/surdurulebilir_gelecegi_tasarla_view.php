<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sürdürülebilirlik Bölümü - Rönesans">
    <meta name="keywords" content="Sürdürülebilirlik Bölümü, Rönesans">

    <title>Sürdürülebilirlik Bölümü - Rönesans</title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

	<div class="form-area">
		
		<div class="container">

			<form action="<?=base_url()?>surdurulebilir_gelecegi_tasarla/basvuru" method="post" autocomplete="off">

				<input name="category" type="hidden" value="architecture">

				<div class="row">

					<div class="col-md-12">
						<?php if ($check_column): ?>
						<?php foreach ($check_column as $value): ?>
							<div class="alert special-alert alert-danger">
	                            <span><?=$value?></span>
	                        </div>
						<?php endforeach ?>
						<?php endif ?>
					</div>
				
					<div class="col-md-12">
						<div class="special-form-group <?=form_error('group_name') ? 'has-error' : ''?>">
							<label class="special-label">Grup Adı</label>
							<input type="text" name="group_name" id="group-name" class="special-input" value="<?=set_value('group_name')?>"/>
							<?=form_error('group_name', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="special-form-group <?=form_error('password') ? 'has-error' : ''?>">
							<label class="special-label">Şifre</label>
							<input type="password" name="password" id="password" class="special-input" value="<?=set_value('password')?>"/>
							<?=form_error('password', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="special-form-group <?=form_error('password_again') ? 'has-error' : ''?>">
							<label class="special-label">Şifre Tekrar</label>
							<input type="password" name="password_again" id="password-again" class="special-input" value="<?=set_value('password_again')?>"/>
							<?=form_error('password_again', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="special-form-group <?=form_error('person_name') ? 'has-error' : ''?>">
							<label class="special-label">İsim</label>
							<input type="text" name="person_name" id="person-name" class="special-input" value="<?=set_value('person_name')?>"/>
							<?=form_error('person_name', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="special-form-group <?=form_error('person_surname') ? 'has-error' : ''?>">
							<label class="special-label">Soyisim</label>
							<input type="text" name="person_surname" id="person-surname" class="special-input" value="<?=set_value('person_surname')?>"/>
							<?=form_error('person_surname', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="special-form-group <?=form_error('identification_no') ? 'has-error' : ''?>">
							<label class="special-label">T.C. Kimlik No</label>
							<input type="text" name="identification_no" id="identification-no" class="special-input" value="<?=set_value('identification_no')?>"/>
							<?=form_error('identification_no', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="special-form-group <?=form_error('university') ? 'has-error' : ''?>">
							<label class="special-label">Üniversite</label>
							<input type="text" name="university" id="university" class="special-input" value="<?=set_value('university')?>"/>
							<?=form_error('university', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="special-form-group <?=form_error('department') ? 'has-error' : ''?>">
							<label class="special-label">Bölüm</label>
							<input type="text" name="department" id="department" class="special-input" value="<?=set_value('department')?>"/>
							<?=form_error('department', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="special-form-group <?=form_error('class') ? 'has-error' : ''?>">
							<label class="special-label">Sınıf</label>
							<input type="text" name="class" id="class" class="special-input" value="<?=set_value('class')?>"/>
							<?=form_error('class', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="special-form-group <?=form_error('phone') ? 'has-error' : ''?>">
							<label class="special-label">Cep Telefonu</label>
							<input type="text" name="phone" id="phone" class="special-input" value="<?=set_value('phone')?>"/>
							<?=form_error('phone', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="special-form-group <?=form_error('email') ? 'has-error' : ''?>">
							<label class="special-label">E-Posta</label>
							<input type="text" name="email" id="email" class="special-input" value="<?=set_value('email')?>"/>
							<?=form_error('email', '<span class="special-help">', '</span>')?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="special-btn-area">
							<button type="submit" class="special-btn">
	                            KAYDET
	                        </button>
						</div>
					</div>

				</div>

			</form>

		</div>

	</div>

  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

</body>
	
</html>
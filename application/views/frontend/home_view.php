<!DOCTYPE html>
<html lang="en">
	
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Sürdürülebilirlik Bölümü - Rönesans">
    <meta name="keywords" content="Sürdürülebilirlik Bölümü, Rönesans">

    <title>Sürdürülebilirlik Bölümü - Rönesans</title>

    <?php include('inc/head.php'); ?>

</head>

<body>
	
	<?php include('inc/nav_top.php'); ?>

  
	<?php include('inc/footer.php'); ?>
	<?php include('inc/script.php'); ?>

</body>
	
</html>
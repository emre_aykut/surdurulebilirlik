<link rel="shortcut icon" href="<?=base_url()?>assets/frontend/img/fav-icon.png">

<link href="<?php echo base_url()?>assets/plugins/bootstrap-v4/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontend/css/animate.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,600,700&subset=latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i&amp;subset=latin-ext" rel="stylesheet">
<link href="<?php echo base_url()?>assets/frontend/css/style.css" rel="stylesheet">

<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
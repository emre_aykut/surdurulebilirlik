<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group_model extends CI_Model {

	var $table = 'groups';
	var $column_order = array('name', 'person_name', 'person_name', 'status', null);
	var $column_search = array('name', 'person_name', 'person_name', 'status',); 
	var $order = array('id' => 'desc');

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function record_check_by_column($field = '', $value = '')
    {
        $query = $this->db->where($field, $value)->get($this->table);

        if($query->num_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}
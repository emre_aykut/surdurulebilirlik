<?php 

function l($key)
{
	$ci =& get_instance();
	$language = $ci->session->userdata("lang_code");


	if(!$language)
	{
		$language = "tr";
	}

	if($language == "tr")
	{
		$lang['test'] = 'Test tr';
	}
	elseif($language == "en")
	{
		$lang['test'] = 'Test en';
	}

	return $lang[$key];
}

function get_lang()
{
	$ci =& get_instance();
	$language = $ci->session->userdata("lang");

	if(!$language){
		return 1;
	}else{
		return $language;
	}
}


?>
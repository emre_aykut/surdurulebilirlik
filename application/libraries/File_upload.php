<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_upload {

	public function do_upload($file = '', $type = 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|zip|rar|svg', $size='0', $path = './upload/')
	{
		$CI =& get_instance();

		$config['upload_path']          = './upload/';
        $config['allowed_types']        = 'svg|pdf|doc|docx|jpg|gif|png|jpeg|JPG|PNG';
        $config['max_size']             = 10000; 
        $config['max_width']            = 10000;
        $config['max_height']           = 10000;

        $CI->load->library('upload', $config);

        $files = $_FILES[$file];
        $filename = $files['name'];
        $new_name = str_replace(array('(', ')'), '', $filename);

        $ext = substr($filename,strrpos($filename,'.',-1),strlen($filename));
        $hash = md5(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, rand(1,10))) . '_-_' . $new_name;
        
        $_FILES[$file]['name'] = $hash.$ext;
        $_FILES[$file]['type'] = $files['type'];
        $_FILES[$file]['tmp_name'] = $files['tmp_name'];
        $_FILES[$file]['error'] = $files['error'];
        $_FILES[$file]['size'] = $files['size'];

        $CI->upload->initialize($config);

        if(!$CI->upload->do_upload($file))
        {
            $data['inputerror'][] = $file;
			$data['error_string'][] = 'Upload error: '.$CI->upload->display_errors('','');
			$data['status'] = FALSE;
			echo json_encode($data);
			exit();
		}

		return $CI->upload->data('file_name');
		
	}

	public function do_upload_multiple($file = '', $type = 'gif|jpg|jpeg|png|pdf|doc|docx|xls|xlsx|zip|rar|svg', $size='0', $path = './upload/')
	{
		$CI =& get_instance();

		$number_of_files = sizeof($_FILES[$file]['tmp_name']);
        $files = $_FILES[$file];

		$config['upload_path']          = './upload/';
        $config['allowed_types']        = 'svg|pdf|doc|docx|jpg|gif|png|jpeg|JPG|PNG';
        $config['max_size']             = 10000; 
        $config['max_width']            = 10000;
        $config['max_height']           = 10000;

        $CI->load->library('upload', $config);

        for ($i = 0; $i < $number_of_files; $i++)
        {
            $filename = $files['name'][$i];
            $new_name = str_replace(array('(', ')'), '', $filename);

            $ext = substr($filename,strrpos($filename,'.',-1),strlen($filename));
            $hash = md5(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, rand(1,10))) . '_-_' . $new_name;

            $_FILES[$file]['name'] = $hash.$ext;
            $_FILES[$file]['type'] = $files['type'][$i];
            $_FILES[$file]['tmp_name'] = $files['tmp_name'][$i];
            $_FILES[$file]['error'] = $files['error'][$i];
            $_FILES[$file]['size'] = $files['size'][$i];

            $CI->upload->initialize($config);

            if ($CI->upload->do_upload($file))
            {
                $uploaded[$i] = $CI->upload->data();
            }
            else
            {
                return FALSE;
            }
        }

        return $uploaded;
		
	}

}
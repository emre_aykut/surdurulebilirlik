<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Surdurulebilir_gelecegi_tasarla extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('surdurulebilir/group_model', 'group');
		$this->load->helper('security');
	}

	public function index()
	{
		echo "homepage";
	}

	public function basvuru()
	{	
		if (!$this->session->userdata('surdurulebilirUserID'))
		{
			$post = $this->input->post(null, true);
			$check_column = array();

			if ($post)
			{
				$this->load->library('form_validation');

				$this->form_validation->set_rules('group_name', 'Grup Adı', 'trim|required|min_length[3]');
				$this->form_validation->set_rules('password', 'Şifre', 'trim|required|min_length[6]');
				$this->form_validation->set_rules('password_again', 'Şifre Tekrar', 'trim|matches[password]');
				$this->form_validation->set_rules('person_name', 'İsim', 'trim|required|min_length[3]');
				$this->form_validation->set_rules('person_surname', 'Soyisim', 'trim|required|min_length[3]');
				$this->form_validation->set_rules('identification_no', 'T.C. Kimlik No', 'trim|required|min_length[11]');
				$this->form_validation->set_rules('university', 'Üniversite', 'trim|required|min_length[3]');
				$this->form_validation->set_rules('department', 'Bölüm', 'trim|required|min_length[3]');
				$this->form_validation->set_rules('class', 'Sınıf', 'trim|required|min_length[1]|max_length[2]');
				$this->form_validation->set_rules('phone', 'Cep Telefonu', 'trim|required|regex_match[/^[0-9]{10}$/]');
				$this->form_validation->set_rules('email', 'E-Posta', 'trim|required|valid_email');

				$this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
				$this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
				$this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
				$this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');
				$this->form_validation->set_message('regex_match', '{field} alanı 10 rakamdan oluşmaktadır. Örn: 5300000000');
				$this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');

				if ($this->form_validation->run() == FALSE) 
				{
					$this->load->view('frontend/surdurulebilir_gelecegi_tasarla_view', array('group' => '', 'check_column' => ''));
				}
				else
				{
					$groupMainData = array(
						'name' => $post['group_name'],
						'password' => password_hash($post['password'], PASSWORD_DEFAULT),
						'category' => $post['category'],
						'application_date' => date('Y-m-d H:i:s', NOW_DATE_TIME),
						'person_name' => $post['person_name'],
						'person_surname' => $post['person_surname'],
						'email' => $post['email'],
						'phone' => $post['phone'],
						'status' => 0,
						'confirmation_token' => substr(md5(rand()), 0, 40),
						'remember_token' => substr(md5(rand()), 0, 40),
						'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
					);

					$groupMainData = $this->security->xss_clean($groupMainData);

					$group_name_check = $this->group->record_check_by_column('name', $groupMainData['name']);
					$email_check = $this->group->record_check_by_column('email', $groupMainData['email']);


					if ($group_name_check == TRUE || $email_check == TRUE)
					{	
						if ($group_name_check)
						{	
							$check_column['group_name'] = 'Girmiş olduğunuz "Grup Adı" başvuru sisteminde mevcut.';
						}

						if ($email_check)
						{
							$check_column['email'] = 'Girmiş olduğunuz "E-Posta" başvuru sisteminde mevcut.';
						}

						$this->load->view('frontend/surdurulebilir_gelecegi_tasarla_view', array('group' => $groupMainData, 'check_column' => $check_column));
					}
					else
					{
						// $insert_id = $this->group->save($groupMainData);

						// $tos = array($groupMainData['email']);
						// $ccs = array();
						// $bccs = array();

						// $html_message = '<h3>Merhaba ' . $groupMainData['person_name'].' '.$groupMainData['person_surname'].',</h3>';
						// $html_message .= '<p>Hesabınızı etkinleştirmeye hazırız. Tek yapmanız gereken, bu e-posta adresinin size ait olduğuna emin olmak.</p>';
						// $html_message .= '<h3><a href="'.base_url().'surdurulebilir_gelecegi_tasarla/basvuru?code='.$groupMainData['confirmation_token'].'&id='.$insert_id.'" style="background: rgb(58, 165, 76); color: rgb(255, 255, 255); padding: 10px 18px; text-decoration: none;">Adresi Doğrula</a></h3><br />';
						// $html_message .= '<div style="style="float:left; margin-top:35px"">Rönesans - Sürdürülebilirlik Bölümü</div>';

						// $status = $this->send_email($tos, $ccs, $bccs, 'Rönesans - Sürdürülebilirlik Bölümü','Rönesans - Sürdürülebilirlik Bölümü Hesap Doğrulaması', $html_message);
				        $status = 1;

				        if ($status == 1)
				        {
				        	$this->session->set_flashdata('register_status', 'Kayıt işleminiz onaylanması için email adresinize gelen maildeki linke tıklayınız.');
				        }
				        else
				        {
				        	// $this->load->model('setting/email_warning_model', 'email_warning');
				        	// $data = array(
				        	// 	'message' => 'Kayıt olurken mail gönderiminde hata oluştu',
				        	// 	'which_page' => 'aday/register',
				        	// 	'which_page_id' => 1,
				        	// 	'user_id' => $insert_id,
				        	// 	'created_at' => date('Y-m-d H:i:s', NOW_DATE_TIME),
				        	// 	'ip_address' => $this->input->ip_address(),
				        	// );
				        	// $this->email_warning->add($data);

				        	$this->session->set_flashdata('register_status', 'Kayıt işleminiz başarısız oldu. Lütfen tekrar deneyiniz.');
				        }

				        redirect(base_url());
					}
				}
			}
			else
			{
				$this->load->view('frontend/surdurulebilir_gelecegi_tasarla_view', array('group' => '', 'check_column' => ''));
			}
		}
		else
		{
			redirect(base_url());
		}
	}

}


?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_lang_id()
	{
		$lang = $this->session->userdata("lang");

		if(!$lang)
		{
			$this->load->model('language/language_model', 'language');
			$default_lang = $this->language->get_lang_default();

			return $default_lang->id;
		}
		else
		{
			return $this->session->userdata("lang");
		}
	}

	public function autorun()
	{	
		// code
	}

	public function send_email($tos=array(), $ccs=array(), $bccs=array(), $sender_name='', $subject='', $message='', $attachments = array())
    {
        require_once APPPATH . 'third_party/phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

        //$mail->SMTPDebug = 3;

        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->Host = 'srvc99.turhost.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@elebul.com';
        $mail->Password = 'Jh23UyV6';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;

        $mail->setFrom('noreply@elebul.com', $sender_name);

        foreach ($tos as $to){
            if ($to){
                $mail->addAddress($to);
            }
        }

        $mail->addReplyTo('noreply@elebul.com', $sender_name);

        foreach ($ccs as $cc){
            if ($cc){
                $mail->addCC($cc);
            }
        }

        foreach ($bccs as $bcc){
            if ($bcc){
                $mail->addBCC($bcc);
            }

        }

        foreach ($attachments as $attachment){
            $mail->addAttachment($attachment['file_with_path']);
        }


        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);


        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }

    }

}





?>
var astraTooltipForLineChart = function (tooltip) {
    console.log(tooltip);
    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft;

    $(".lineChartTooltip").css({
        opacity: 0,
    });



    //console.log(tooltip.dataPoints);

    //if(typeof tooltip.dataPoints[0] !== 'undefined'){
    if(typeof tooltip.x !== 'undefined'){
        var content = tooltip.yLabel;

        $(".lineChartTooltip").html(content);
        $(".lineChartTooltip").css({
            opacity: 1,
            top: positionY + tooltip.y - 20 + "px",
            left: positionX + tooltip.x + "px",
        });
    }

    return "";
};

var astraTooltipForBarChart = function (tooltip) {
    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft;

    $(".barChartTooltip").css({
        opacity: 0,
    });
    $(".barChartTotal").css({
        opacity: 0,
    });

    if (!tooltip || !tooltip.opacity) {
        return;
    }

    if (tooltip.dataPoints.length > 0) {
        var total = 0;
        var lastX = 0;
        tooltip.dataPoints.forEach(function (dataPoint) {
            var content = dataPoint.yLabel;
            total = total + parseInt(dataPoint.yLabel);
            var $tooltip = $("#tooltip-" + dataPoint.datasetIndex);

            $tooltip.html(content);
            $tooltip.css({
                opacity: 1,
                top: positionY + dataPoint.y + "px",
                left: positionX + dataPoint.x + "px"
            });
            lastX = dataPoint.x;
        });
        var $barChartTotal = $(".barChartTotal");
        $barChartTotal.html('Total: ' + total);
        $barChartTotal.css({
            opacity: 1,
            top: positionY + $(this._chart.canvas).height() - 55 + "px",
            left: positionX + lastX + "px"
        });
    }
};

var lineChart = new Chart('lineChart', {
    type: 'line',
    data: {
        labels: [
            "1 Ocak","2 Ocak","3 Ocak","4 Ocak",
            "5 Ocak","6 Ocak","7 Ocak","8 Ocak",
            "9 Ocak","10 Ocak","11 Ocak","12 Ocak",
            "13 Ocak","14 Ocak","15 Ocak",
            "16 Ocak","17 Ocak","18 Ocak","19 Ocak",
            "20 Ocak","21 Ocak","22 Ocak","23 Ocak",
            "24 Ocak","25 Ocak","26 Ocak","27 Ocak",
            "28 Ocak","29 Ocak","30 Ocak"
        ],
        datasets: [{
            label: 'Buraya Başlık / default olarak chartScript.JS dosyasından',
            data: [12, 4, 2, 12, 4, 10, 11, 8, 5, 5, 3, 11, 7, 4, 5, 9, 4, 12, 8, 5, 10, 8, 8, 6, 12, 3, 3, 8, 6, 7],
            backgroundColor: [
                'rgba(46, 204, 103, 0.3)'
            ],
            borderColor: [
                '#2ecc71'
            ],
            pointRadius: 5,
            pointBackgroundColor: 'rgba(255, 255, 255, 0)',
            pointBorderColor: 'rgba(255, 255, 255, 0)',
            //pointHoverRadius: 20,
            //pointHoverBackgroundColor: '#2ecc71',
            //pointHoverBorderColor: '#ffffff',
            //pointBorderWidth: 4
        }]
    },
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [
                {
                    ticks: {
                        stepValue: 1,
                        max: 15,
                        min: 1
                    }
                }
            ],
            xAxes: [
                {
                    ticks: {
                        stepSize: 2,
                        autoSkip: true
                    }
                }
            ]
        },
        tooltips: {
            enabled: true,
            mode: 'index',
            intersect: false,
            backgroundColor: 'transparent',
            //custom: astraTooltipForLineChart,
            callbacks: {
                title: function () {
                    return " "
                },
                label: astraTooltipForLineChart
            }
        }
    }
});


var barChart = new Chart('barChart', {
    type: 'bar',
    data: {
        labels: [
            "18-24",
            "25-34",
            "35-44",
            "45-54",
            "55-64",
            "65+"
        ],
        datasets: [
            {
                label: '# Birincil Seçim',
                data: [3, 19, 5, 5, 2, 3],
                backgroundColor: '#3498db'
            },
            {
                label: '# İkincil Seçim',
                data: [4, 2, 3, 5, 2, 3],
                backgroundColor: '#1de3eb'
            }
        ]

    },
    options: {
        scales: {
            xAxes: [
                {
                    stacked: true,
                    gridLines: {
                        color: "transparent"
                    }
                }
            ],
            yAxes: [
                {
                    stacked: true
                }
            ]
        },
        tooltips: {
            enabled: false,
            mode: 'index',
            intersect: false,
            custom: astraTooltipForBarChart
        }

    }
});

var doughnutChart = new Chart('doughnutChart', {
    type: 'doughnut',
    astraLabel: true,
    data: {
        datasets: [{
            labels: ["a", "b"],
            data: [25, 75],
            backgroundColor: [
                "#ff6f59",
                "#efeff4"
            ]
        }]
    },
    options: {
        tooltips: {
            enabled: false,
            mode: 'single',
            intersect: false
        },
        maintainAspectRatio: false,
        cutoutPercentage: 80
    }
});

var doughnutChart2 = new Chart('doughnutChart2', {
    type: 'doughnut',
    astraLabel: true,
    data: {
        datasets: [{
            labels: ["a", "b"],
            data: [50, 50],
            backgroundColor: [
                "#2ecc71",
                "#efeff4"
            ]
        }]
    },
    options: {
        tooltips: {
            enabled: false,
            mode: 'index',
            intersect: false
        },
        maintainAspectRatio: false,
        cutoutPercentage: 80
    }
});

var doughnutChart3 = new Chart('doughnutChart3', {
    type: 'doughnut',
    astraLabel: true,
    data: {
        datasets: [{
            labels: ["a", "b"],
            data: [75, 25],
            backgroundColor: [
                "#01bbc3",
                "#efeff4"
            ]
        }]
    },
    options: {
        tooltips: {
            enabled: false,
            mode: 'index',
            intersect: false
        },
        maintainAspectRatio: false,
        cutoutPercentage: 80
    }
});

Chart.plugins.register({
    afterDatasetsDraw: function(chart, easing) {
        //console.log(chart);
        if(chart.config.astraLabel !== 'undefined' && chart.config.astraLabel){
            var label = $("#"+chart.canvas.id).parent("div").children(".astraLabel");
            label.html(chart.config.data.datasets[0].data[0] + "%");
            var heightOfChart = $("#"+chart.canvas.id).height();
            label.css(
                {
                    "margin-top": -heightOfChart / 2,
                    "font-size": heightOfChart / 5,
                    "line-height": heightOfChart / 10 + "px"
                }
            );
            //$("#doughnutChartLabel").html("aaaa");
        }
    }
});


/**
 * DUMMY DATA
 */

var allData = {
    "30" : [12, 4, 2, 12, 4, 10, 11, 8, 5, 5, 3, 11, 7, 4, 5, 9, 4, 12, 8, 5, 10, 8, 8, 6, 12, 3, 3, 8, 6, 7],
    "60" : [12, 4, 2, 12, 4, 10, 11, 8, 5, 5, 3, 11, 7, 4, 5, 9, 4, 12, 8, 5, 10, 8, 8, 6, 12, 3, 3, 8, 6, 7,
        8, 5, 5, 10, 6 ,2, 7, 11, 9, 5, 4, 11, 10, 8, 12, 10, 2, 8, 4 ,4 ,4 ,5 ,3 ,9, 9, 6, 7 ,5 ,11 ,3],
    "90" : [12, 4, 2, 12, 4, 10, 11, 8, 5, 5, 3, 11, 7, 4, 5, 9, 4, 12, 8, 5, 10, 8, 8, 6, 12, 3, 3, 8, 6, 7,
        8, 5, 5, 10, 6 ,2, 7, 11, 9, 5, 4, 11, 10, 8, 12, 10, 2, 8, 4 ,4 ,4 ,5 ,3 ,9, 9, 6, 7 ,5 ,11 ,3,
        7, 10, 7, 9, 12, 3 ,7, 8, 4, 7, 8, 6, 11, 9, 8, 5, 4, 12, 11, 9, 3, 5 ,10 ,5 ,10, 12 ,2, 8, 5 ,8]
};
var allLabels = {
    "30" : [
        "1 Ocak","2 Ocak","3 Ocak","4 Ocak","5 Ocak","6 Ocak","7 Ocak","8 Ocak","9 Ocak","10 Ocak","11 Ocak","12 Ocak",
        "13 Ocak","14 Ocak","15 Ocak","16 Ocak","17 Ocak","18 Ocak","19 Ocak","20 Ocak","21 Ocak","22 Ocak","23 Ocak",
        "24 Ocak","25 Ocak","26 Ocak","27 Ocak","28 Ocak","29 Ocak","30 Ocak"
    ],
    "60" : [
        "1 Ocak","2 Ocak","3 Ocak","4 Ocak","5 Ocak","6 Ocak","7 Ocak","8 Ocak","9 Ocak","10 Ocak","11 Ocak","12 Ocak",
        "13 Ocak","14 Ocak","15 Ocak","16 Ocak","17 Ocak","18 Ocak","19 Ocak","20 Ocak","21 Ocak","22 Ocak","23 Ocak",
        "24 Ocak","25 Ocak","26 Ocak","27 Ocak","28 Ocak","29 Ocak","30 Ocak",
        "1 Ocak","2 Ocak","3 Ocak","4 Ocak","5 Ocak","6 Ocak","7 Ocak","8 Ocak","9 Ocak","10 Ocak","11 Ocak","12 Ocak",
        "13 Ocak","14 Ocak","15 Ocak","16 Ocak","17 Ocak","18 Ocak","19 Ocak","20 Ocak","21 Ocak","22 Ocak","23 Ocak",
        "24 Ocak","25 Ocak","26 Ocak","27 Ocak","28 Ocak","29 Ocak","30 Ocak"
    ],
    "90" : [
        "1 Ocak","2 Ocak","3 Ocak","4 Ocak","5 Ocak","6 Ocak","7 Ocak","8 Ocak","9 Ocak","10 Ocak","11 Ocak","12 Ocak",
        "13 Ocak","14 Ocak","15 Ocak","16 Ocak","17 Ocak","18 Ocak","19 Ocak","20 Ocak","21 Ocak","22 Ocak","23 Ocak",
        "24 Ocak","25 Ocak","26 Ocak","27 Ocak","28 Ocak","29 Ocak","30 Ocak",
        "1 Ocak","2 Ocak","3 Ocak","4 Ocak","5 Ocak","6 Ocak","7 Ocak","8 Ocak","9 Ocak","10 Ocak","11 Ocak","12 Ocak",
        "13 Ocak","14 Ocak","15 Ocak","16 Ocak","17 Ocak","18 Ocak","19 Ocak","20 Ocak","21 Ocak","22 Ocak","23 Ocak",
        "24 Ocak","25 Ocak","26 Ocak","27 Ocak","28 Ocak","29 Ocak","30 Ocak",
        "1 Ocak","2 Ocak","3 Ocak","4 Ocak","5 Ocak","6 Ocak","7 Ocak","8 Ocak","9 Ocak","10 Ocak","11 Ocak","12 Ocak",
        "13 Ocak","14 Ocak","15 Ocak","16 Ocak","17 Ocak","18 Ocak","19 Ocak","20 Ocak","21 Ocak","22 Ocak","23 Ocak",
        "24 Ocak","25 Ocak","26 Ocak","27 Ocak","28 Ocak","29 Ocak","30 Ocak"
    ],
}

/**
 * Bu kısım backend'e sorgu yollayarak çalışıp tekrar yazılacağı için üzerinde uzun uzun durulmaksızın yazıldı. Sorguları
 * en nihayetinde API'nin bizden istediği şekilde yazacağız.
 */
$(function () {

    $(".changeDate").click(function () {
        var dateVal = $(this).attr('data-dateVal').toString();
        console.log(window.lineChart.data.datasets[0]);
        console.log(window.allLabels[dateVal]);
        window.lineChart.data.labels = window.allLabels[dateVal];
        window.lineChart.update();
        window.lineChart.data.datasets[0].data = window.allData[dateVal];
        window.lineChart.update();
        $('#lineChart').trigger('hover');
        //console.log(window.lineChart);
    });
});
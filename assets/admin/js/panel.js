$(document).ready(function(){
	var pageActiveUrl = window.location.pathname;
	pageActiveUrl = pageActiveUrl.replace(/\//g,'-');
	var pageActiveUrlArray = pageActiveUrl.split('-');

	$('#' + pageActiveUrl).parent().addClass('active');

	if (pageActiveUrlArray[2] == "settings") 
	{
		$('#-admin-settings-dashboard').parent().addClass('active');
	}

	$('.menu-open').click(function(){
		$('.sidebar').attr('style','width: 100%; min-width: 100%;');
		$('.sidebar #leftside-navigation ul li a span').css('display','inline-block');
		$(this).css('display','none');
		$('.menu-close').css('display','block');
        $('.customer-panel-logo').css('display', 'none');
	});

	$('.menu-close').click(function(){
		$('.sidebar').attr('style','width: 7%; min-width: 55px;');
		$('.sidebar #leftside-navigation ul li a span').css('display','none');
		$(this).css('display','none');	
		$('.menu-open').css('display','block');
        $('.customer-panel-logo').css('display', 'block');
	});

});

function replace_special_chars(str) 
{
    var specialChars = [['ş', 's'], ['ğ', 'g'], ['ü', 'u'], ['ı', 'i'],['_', '-'],
        ['ö', 'o'], ['Ş', 'S'], ['Ğ', 'G'], ['Ç', 'C'], ['ç', 'c'],
        ['Ü', 'U'], ['İ', 'I'], ['Ö', 'O'], ['ş', 's']];

    for (var i = 0; i < specialChars.length; i++) {
        str = str.replace(eval('/' + specialChars[i][0] + '/ig'), specialChars[i][1]);
    }
    return str;
}

function ajax_languages()
{
    $.ajax({
        url : base_url + 'admin/settings/language/ajax_language_get',
        type: 'GET',
        dataType: 'JSON',
        success: function(data)
        {
            languages = data.languages;
            default_language = data.default_language.id;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        },
        async: false,
    });
}